import React from "react";
import { Router } from "@reach/router";
import { Layout } from "antd";

import "./App.css";
import Home from "./components/Home";
import Login from "./components/Login";
import Signup from "./components/Signup";
import Nav from "./components/Nav";
function App() {
  const { Header, Footer, Sider, Content } = Layout;

  return (
    <div className="App">
      <Router>
        <Home path="/"></Home>
        <Login path="/Login"></Login>
        <Signup path="/Signup"></Signup>
        <Nav path="/Nav"></Nav>
      </Router>
    </div>
  );
}

export default App;
