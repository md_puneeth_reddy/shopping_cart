/*
{
    id:1
    email:puneeth@gmail.com
    passwprd: puneeth
}



*/

const initialState = {
  users: []
};
const reducer = (state = initialState, action) => {
  if (action.type === ADD_USER) {
    return {
      ...state,
      users: state.users.concat(action.payload)
    };
  }
  return state;
};
export default reducer;
