import { UPDATE_USERS } from "./Actiontypes";

export const adduser = (email, password) => {
  return {
    type: ADD_USER,
    payload: { email: email, password: password }
  };
};

export function signup(email, password) {
  return dispatch => {
    dispatch(adduser(email, password));
  };
}
