import * as React from "react";
import { useState } from "react";
import { Link } from "@reach/router";
export interface Props {}

const Nav: React.SFC<Props> = () => {
  return (
    <div>
      <Link to="/">Home</Link>&nbsp;
      <Link to="/Login">Login</Link>&nbsp;
      <Link to="/Signup">Signup</Link>&nbsp;
    </div>
  );
};

export default Nav;
