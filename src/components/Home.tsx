import * as React from "react";
import Nav from "./Nav";
import { useState } from "react";
import { Layout } from "antd";
export interface Props {}

const Home: React.SFC<Props> = () => {
  const { Header, Footer, Sider, Content } = Layout;
  return (
    <div>
      <Layout>
        <Header>
          <Nav />
        </Header>
        <Layout>
          <Content>
            <h1>Welcometo Novartis</h1>
          </Content>
        </Layout>
        {/* <Footer>Footer</Footer> */}
      </Layout>
    </div>
  );
};

export default Home;
