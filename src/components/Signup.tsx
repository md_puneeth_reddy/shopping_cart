import * as React from "react";
import { useState } from "react";
import Nav from "./Nav";
import { navigate } from "@reach/router";

export interface Props {}

const Signup: React.SFC<Props> = () => {
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");
  const [msg, setmsg] = useState("");
  const validate = () => {
    if (email == "" || password == "") {
      setmsg("not success");
    } else {
      if (
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
          email
        )
      ) {
        setmsg("Success");

        //here we need to add to the users
      } else {
        setmsg("You have entered an invalid email address!");
      }
    }
  };
  return (
    <div>
      <Nav />
      <form>
        <table>
          <tr>
            <td>
              <label>Email</label>
            </td>
            <td>
              <input
                type="text"
                name="email"
                value={email}
                onChange={e => {
                  setemail(e.target.value);
                }}
              ></input>
            </td>
          </tr>

          <tr>
            <td>
              <label>password</label>
            </td>
            <td>
              <input
                type="password"
                name="password"
                value={password}
                onChange={e => {
                  setpassword(e.target.value);
                }}
              ></input>
            </td>
          </tr>
        </table>
        <button onClick={validate}>Signup</button>
      </form>
      {msg}
    </div>
  );
};

export default Signup;
