import * as React from "react";
import { useState } from "react";
import Nav from "./Nav";
import { navigate } from "@reach/router";
import { Layout, Checkbox } from "antd";

export interface Props {}

const Login: React.SFC<Props> = () => {
  const { Header, Footer, Sider, Content } = Layout;
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");
  const [msg, setmsg] = useState("");
  const validate = () => {};
  return (
    <div>
      <Layout>
        <Header>
          <Nav />
        </Header>
        <Layout>
          <Content>
            <form>
              <table>
                <tr>
                  <td>
                    <label>Email</label>
                  </td>
                  <td>
                    <input
                      type="text"
                      name="email"
                      value={email}
                      onChange={e => {
                        setemail(e.target.value);
                      }}
                    ></input>
                  </td>
                </tr>

                <tr>
                  <td>
                    <label>password</label>
                  </td>
                  <td>
                    <input
                      type="password"
                      name="password"
                      value={password}
                      onChange={e => {
                        setpassword(e.target.value);
                      }}
                    ></input>
                  </td>
                </tr>
              </table>

              <button onClick={validate}>Login</button>
            </form>
            {msg}
          </Content>
        </Layout>
        {/* <Footer>Footer</Footer> */}
      </Layout>
    </div>
  );
};

export default Login;
